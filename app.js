'use strict';

const port = process.env.PORT || 1337;
const express = require('express');
const app = express();
const compression = require('compression');
const http = require('http');
const server = http.createServer(app);

app
    .use ( compression() )
    .use ( express.static(__dirname + '/public') )
    .get ( '/', (req, res) => res.redirect('index.html') )
    .get ( '*.html', (req, res) => res.redirect('index.html#/404') )
    .all ( '*', (req, res, next) => next(new Error('Service not exists')) )
    .use ( (err, req, res, next) => res.status(200).json({err: err.message}) );

server.listen(port, () => {console.log(`${new Date()} Listening at ${port}`);});
