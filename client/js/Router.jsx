'use strict';

const React = require('react');
const {Router: ReactRouter, Route, Redirect, useRouterHistory} = require('react-router');

const Layout = require('components/layout/Layout');
const Main = require('views/main/Main');
const NotFound = require('views/notFound/NotFound');

const {createHashHistory} =require('history');
const appHistory = useRouterHistory(createHashHistory)({ queryKey: false });

const Router = React.createClass({
    render() {
        return (
            <ReactRouter history={appHistory}>
                <Route component={Layout}>
                    <Route path="/main" component={Main}/>
                    <Route path="/404" component={NotFound}/>
                </Route>
                <Redirect from="/" to="/main"/>
                <Redirect from="*" to="/404"/>
            </ReactRouter>
        );
    }
});

module.exports = Router;
