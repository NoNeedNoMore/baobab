'use strict';

require('react-tap-event-plugin')();

const React = require('react');
const Router = require('Router');
const {render} = require('react-dom');

render(<Router/>, document.getElementById('app'));
