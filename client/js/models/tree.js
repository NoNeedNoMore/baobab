'use strict';

const tree = {
    isUgly: true,
    url: 'https://query.yahooapis.com/v1/public/yql',
    format: 'json',
    env: 'store://datatables.org/alltableswithkeys',
    currencyPairs: '"USDEUR", "USDJPY", "USDBGN", "USDCZK", "USDDKK", "USDGBP", "USDHUF", "USDLTL", "USDLVL", "USDPLN", "USDRON", "USDSEK", "USDCHF", "USDNOK", "USDHRK", "USDRUB", "USDTRY", "USDAUD", "USDBRL", "USDCAD", "USDCNY", "USDHKD", "USDIDR", "USDILS", "USDINR", "USDKRW", "USDMXN", "USDMYR", "USDNZD", "USDPHP", "USDSGD", "USDTHB", "USDZAR", "USDISK"',
    stockSymbols: '"YHOO", "AAPL", "GOOG", "MSFT", "FB", "TSLA", "NFLX", "F", "K", "SIRI", "ZNGA", "BBRY", "MU", "QQQ", "MAR", "CSCO", "BAC", "GE", "TWTR", "PFE", "RKUS", "FCX", "PBR", "CHK", "WLL", "VRX", "PHM", "SWN", "NOK", "MRO", "VALE", "AA", "S" '
};

module.exports = tree;
