'use strict';

const request = require('superagent');
const tree = require('tree');

const {url, format, env, currencyPairs} = tree.get();

function getCurrency() {
    return request
        .get(url)
        .query({
            format,
            env,
            q: `select * from yahoo.finance.xchange where pair in (${currencyPairs})`
        })
        .set('Accept', 'application/json')
        .send()
        .end((err, res) => err ?
            console.log(err) :
            tree.set('currency', res.body)
    );
}

module.exports = getCurrency;
