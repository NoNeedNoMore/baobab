'use strict';

const request = require('superagent');
const tree = require('tree');

const {url, format, env, stockSymbols} = tree.get();

function getStock() {
    return request
        .get(url)
        .query({
            format,
            env,
            q: `select * from yahoo.finance.quote where symbol in (${stockSymbols})`
        })
        .set('Accept', 'application/json')
        .send()
        .end((err, res) => err ?
            console.log(err) :
            tree.set('stock', res.body)
    );
}

module.exports = getStock;
