'use strict';

const request = require('superagent');

const startProcessing = require('actions/startProcessing');
const endProcessing = require('actions/endProcessing');
const errProcessing = require('actions/errProcessing');

function restApi({model, ext = 'json', action, id = '', reducer, payload} = {}) {
    return (dispatch, getState) => {
        dispatch(startProcessing({model, ext, action, id, reducer}));
        payload = payload || getState()[reducer] && getState()[reducer].payload || {};
        return request
            .post(`/restApi/${model}.${ext}/${action}/${id}`)
            .set('Accept', 'application/json')
            .send(payload)
            .end((err, res) => dispatch(err ?
                errProcessing(err) :
                endProcessing({model, ext, action, id, reducer, body: res.body})
            ));
    };
}

module.exports = restApi;
