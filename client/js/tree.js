'use strict';

const Baobab = require('baobab');
const treeModel = require('models/tree');

const tree = new Baobab(treeModel);

module.exports = tree;
