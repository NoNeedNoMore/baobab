'use strict';

const React = require('react');
const {Card, CardTitle, CardText} = require('material-ui');
const {Link} = require('react-router');

const NotFound = React.createClass({
    render() {
        return (
            <Card style={{float: 'left'}}>
                <CardTitle title="404 error" subtitle="page not found" />
                <CardText>We are sorry but the page you are looking for does not exist<br />
                    You could return to the <Link to="#/main">homepage</Link>
                </CardText>
            </Card>
        );
    }
});

module.exports = NotFound;
