'use strict';

const React = require('react');
const tree = require('tree');
const getStockAction = require('actions/getStock');
const getCurrencyAction = require('actions/getCurrency');

const StockSymbol = require('components/stockSymbol/StockSymbol');
const CurrencySymbol = require('components/currencySymbol/CurrencySymbol');

const delay = 500;

const Main = React.createClass({
    getInitialState() {
        return {};
    },

    componentWillMount() {
        tree.on('update', this.handleTreeChange);
        getStockAction();
        getCurrencyAction();
    },

    componentWillUnmount() {
        tree.off(this.handleTreeChange);
    },

    handleTreeChange(e) {
        this.setState(e.data.currentData);
        if (tree.get().stock && tree.get().currency) {
            tree.off(this.handleTreeChange);
            (function updateData() {
                getStockAction();
                getCurrencyAction();
                setTimeout(updateData, delay);
            })();
        }
    },
    
    render() {
        const currency = this.state.currency;
        const stock = this.state.stock;
        return (
            <div>
                {!stock ? 
                    null : 
                    stock.query.results.quote.map((quote, i) => <StockSymbol {...quote} key={quote.symbol} i={i}/>)
                }
                <br/>
                {!currency ? 
                    null : 
                    currency.query.results.rate.map((rate, i) => <CurrencySymbol {...rate} key={rate.id} i={i}/>)
                }
            </div>
        );
    }
});

module.exports = Main;
