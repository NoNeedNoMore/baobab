'use strict';

const {createStore, combineReducers, applyMiddleware} = require('redux');
const thunkMiddleware = require('redux-thunk').default;

const net = require('reducers/net');

const store = createStore(
    combineReducers({
        net
    }),
    applyMiddleware(thunkMiddleware)
);

module.exports = store;
