'use strict';

const React = require('react');
const tree = require('tree');

const {Card, CardTitle, CardText, List, ListItem} = require('material-ui');

const style = {
    display: 'inline-block',
    margin: 5,
    width: 300
};

const {isUgly} = tree.get();

const StockSymbol = React.createClass({
    propTypes: {
        symbol: React.PropTypes.string,
        AverageDailyVolume: React.PropTypes.string,
        Change: React.PropTypes.string,
        DaysLow: React.PropTypes.string,
        DaysHigh: React.PropTypes.string,
        YearLow: React.PropTypes.string,
        YearHigh: React.PropTypes.string,
        MarketCapitalization: React.PropTypes.string,
        LastTradePriceOnly: React.PropTypes.string,
        DaysRange: React.PropTypes.string,
        Name: React.PropTypes.string,
        Symbol: React.PropTypes.string,
        Volume: React.PropTypes.string,
        StockExchange: React.PropTypes.string,
        i: React.PropTypes.number
    },
    
    getInitialState() {
        return {};
    },

    componentWillMount() {
        this.stockSymbolCursor = tree.select('stock', 'query', 'results', 'quote', this.props.i);
        this.stockSymbolCursor.on('update', this.handleTreeChange);
    },

    componentWillUnmount() {
        this.stockSymbolCursor.off(this.handleTreeChange);
    },

    handleTreeChange(e) {
        e.data.currentData && this.setState(e.data.currentData);
    },
    
    render() {
        let props = this.state.symbol ? this.state : this.props;
        return (
            isUgly ? 
                (<div style={Object.assign({}, style, {border: '1px solid black', padding: 10})}>
                    <p>{this.props.Symbol} {this.props.Name}</p>
                    <p>AverageDailyVolume {props.AverageDailyVolume || '-'}</p>
                    <p>Change {props.Change || '-'}</p>
                    <p>DaysLow {props.DaysLow || '-'}</p>
                    <p>DaysHigh {props.DaysHigh || '-'}</p>
                    <p>YearLow {props.YearLow || '-'}</p>
                    <p>YearHigh {props.YearHigh || '-'}</p>
                    <p>MarketCapitalization {props.MarketCapitalization || '-'}</p>
                    <p>LastTradePriceOnly {props.LastTradePriceOnly || '-'}</p>
                    <p>DaysRange {props.DaysRange || '-'}</p>
                    <p>Volume {props.Volume || '-'}</p>
                    <p>StockExchange {props.StockExchange || '-'}</p>
                </div>) :
                (<Card style={style}>
                    <CardTitle title={this.props.Symbol} subtitle={this.props.Name} />
                    <CardText>
                        <List>
                            <ListItem primaryText="AverageDailyVolume" secondaryText={props.AverageDailyVolume || '-'} />
                            <ListItem primaryText="Change" secondaryText={props.Change || '-'} />
                            <ListItem primaryText="DaysLow" secondaryText={props.DaysLow || '-'} />
                            <ListItem primaryText="DaysHigh" secondaryText={props.DaysHigh || '-'} />
                            <ListItem primaryText="YearLow" secondaryText={props.YearLow || '-'} />
                            <ListItem primaryText="YearHigh" secondaryText={props.YearHigh || '-'} />
                            <ListItem primaryText="MarketCapitalization" secondaryText={props.MarketCapitalization || '-'} />
                            <ListItem primaryText="LastTradePriceOnly" secondaryText={props.LastTradePriceOnly || '-'} />
                            <ListItem primaryText="DaysRange" secondaryText={props.DaysRange || '-'} />
                            <ListItem primaryText="Volume" secondaryText={props.Volume || '-'} />
                            <ListItem primaryText="StockExchange" secondaryText={props.StockExchange || '-'} />
                        </List>
                    </CardText>
                </Card>)
        );
    }
});

module.exports = StockSymbol;
