'use strict';

const React = require('react');
const tree = require('tree');

const {Card, CardTitle, CardText, List, ListItem} = require('material-ui');

const style = {
    display: 'inline-block',
    margin: 5,
    width: 150
};

const {isUgly} = tree.get();

const MarketSymbol = React.createClass({
    propTypes: {
        id: React.PropTypes.string,
        Name: React.PropTypes.string,
        Rate: React.PropTypes.string,
        'Date': React.PropTypes.string,
        Time: React.PropTypes.string,
        Bid: React.PropTypes.string,
        Ask: React.PropTypes.string
    },
    
    getInitialState() {
        return {};
    },

    componentWillMount() {
        this.currencySymbolCursor = tree.select('currency', 'query', 'results', 'rate', this.props.i);
        this.currencySymbolCursor.on('update', this.handleTreeChange);
    },

    componentWillUnmount() {
        this.currencySymbolCursor.off(this.handleTreeChange);
    },

    handleTreeChange(e) {
        e.data.currentData && this.setState(e.data.currentData);
    },
    
    render() {
        let props = this.state.id ? this.state : this.props;
        return (
            isUgly ?
                (<div style={Object.assign({}, style, {border: '1px solid black', padding: 10})}>
                    <p>{this.props.Name} currency</p>
                    <p>Rate {props.Rate || '-'}</p>
                    <p>Date {props.Date || '-'}</p>
                    <p>Time {props.Time || '-'}</p>
                    <p>Bid {props.Bid || '-'}</p>
                    <p>Ask {props.Ask || '-'}</p>
                </div>) :
                (<Card style={style}>
                    <CardTitle title={this.props.Name} subtitle="currency" />
                    <CardText>
                        <List>
                            <ListItem primaryText="Rate" secondaryText={props.Rate || '-'} />
                            <ListItem primaryText="Date" secondaryText={props.Date || '-'} />
                            <ListItem primaryText="Time" secondaryText={props.Time || '-'} />
                            <ListItem primaryText="Bid" secondaryText={props.Bid || '-'} />
                            <ListItem primaryText="Ask" secondaryText={props.Ask || '-'} />
                        </List>
                    </CardText>
                </Card>)
        );
    }
});

module.exports = MarketSymbol;
